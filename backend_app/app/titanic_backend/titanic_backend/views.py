from django.http import HttpResponse, JsonResponse
import json

from . import ML

def index(request):
    formData = request.POST.dict()

    if ML.survive(formData):
        survived = 'yes'
    else:
        survived = 'no'


    response = JsonResponse({'survived':survived})
    response['Access-Control-Allow-Origin'] = '*'
    response["Access-Control-Allow-Methods"] = "GET, OPTIONS, POST"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"
    return response
