import pickle, os

fichier = 'titanic_backend/titanic_backend/basic_model'
loaded_model = pickle.load(open(fichier, 'rb'))


def survive(data):
    print("\n### Data from front ###\n")
    for k,v in data.items():
        print(k +" : "+ v)

    features = data_to_features(data)

    pred = loaded_model.predict(features)[0]

    return bool(pred)


def data_to_features(data):
    #
    # - Create a 2D array with ordered features from input data
    #
    # - Needed features are:
    #
    #       Age, Pclass, Fare, Sex_female, Sex_male,
    #       Embarked_C, Embarked_Q, Embarked_S

    features = []
    # Age, Pclass, Fare
    features.append(int(data['age']))
    features.append(int(data['Pclass']))
    features.append(int(data['price']))

    # Sex_female, Sex_male
    male = (data['gender'] == "male")
    features.append(int(not male))
    features.append(int(male))

    # Embarked_C, Embarked_Q, Embarked_S
    embark = data['embarked'][0] # C, Q or S
    features.append(int( embark == 'C' ))
    features.append(int( embark == 'Q' ))
    features.append(int( embark == 'S' ))

    print(f"\n### Features from data ###\n\n{features}\n")

    return [features]
