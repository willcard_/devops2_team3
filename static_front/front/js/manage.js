sendToDjango = function () {

  var myForm = document.getElementById('myForm');
  formData = new FormData(myForm);

  if (document.getElementsByName('name')[0].value.length == 0) {
    console.log("Name is empty");
    return;
  }

  var bt = document.getElementById('btSubmit');
  bt.disabled = true;

  fetch('http://localhost:8000/',
      { method: 'POST',
      //mode: 'cors',
      body:formData })
    .then( function(response){ return response.json()  })
    .then( function(json){
      console.log(json);
      redirect(json); })
};

redirect = function(json) {
  params = new URLSearchParams(json).toString();
  window.open("result.html?"+params,"_self");
};
